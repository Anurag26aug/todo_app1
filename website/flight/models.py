from django.db import models
from uuid import uuid4
# Create your models here.

class Flight(models.Model):
    f_id = models.CharField(max_length=10)
    src = models.CharField(max_length=10)
    dest = models.CharField(max_length=10)
    fare = models.IntegerField()

    def __repr__(self):
        return """
    Flight ID: {}
    Source: {}
    Destination: {}
    Fare: {}
    """.format(self.f_id, self.src, self.dest, self.fare)

    def __str__(self):
        return self.f_id

    def get_fare(self):
        return self.fare

class Booking(models.Model):
    f_id = models.CharField(max_length=10)
    booking_id = models.CharField(max_length=20)
    payment = models.BooleanField()
    no_of_pass = models.IntegerField()
    child_pass = models.IntegerField()
