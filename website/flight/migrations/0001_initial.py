# Generated by Django 2.1.4 on 2019-01-12 17:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('f_id', models.CharField(max_length=10)),
                ('src', models.CharField(max_length=10)),
                ('dest', models.CharField(max_length=10)),
                ('fare', models.IntegerField()),
            ],
        ),
    ]
