from django.shortcuts import render
from django.http import HttpResponse
import uuid

from .models import Flight, Booking
# Create your views here.


def index(request):
    all_records = Flight.objects.all()
    context = {'records': all_records}

    return render(request, 'flights.html', context)

def about(request):
    return render(request, 'about.html')

def booking(request, f_id):
    f_id = f_id.strip()
    context = {'f_id': f_id}
    context['fare'] = Flight.objects.values_list('fare', flat=True).get(f_id=f_id)
    context['booking_id'] = uuid.uuid4()

    if request.method == "GET":
        context['payment'] = False

        return render(request,'fillup_details.html', context)

    elif request.method == "POST":
        audlts = request.POST['no_of_audlts']
        childrens = request.POST['no_of_childs']
        b = Booking(f_id=f_id, booking_id= context['booking_id'], payment=False,
                    no_of_pass=audlts, child_pass=childrens)
        b.save()
        context['booking_confirmed'] = True
        context['details'] = Booking.objects.filter(booking_id=context['booking_id'])
        print(context)
        return render(request, 'booking_success.html', context)
