from django.shortcuts import render
from django.http import HttpResponse

from .models import Bus
# Create your views here.


def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def booking(request):
    all_records = Bus.objects.all()
    context = {'records': all_records}

    return render(request, 'booking.html', context)

