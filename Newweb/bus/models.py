from django.db import models
class Bus(models.Model):
    f_id = models.CharField(max_length=10)
    src = models.CharField(max_length=10)
    dest = models.CharField(max_length=10)
    fare = models.IntegerField()

    def __repr__(self):
        return """
    Bus ID: {}
    Source: {}
    Destination: {}
    Fare: {}
    """.format(self.f_id, self.src, self.dest, self.fare)

# Create your models here.
